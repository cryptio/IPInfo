"""Accepts an IP and provides useful information such as geo-location and hostname."""

__author__ = 'Encryptio'

from scapy.all import *
import sys
import argparse
import requests
import json
import ipaddress
import time

PARSER = argparse.ArgumentParser(description="Provide useful information for an IP address")
PARSER.add_argument('-f', '--file', help="The PCAP file to read the IP addresses from", type=str)
PARSER.add_argument('-i', '--ipaddr', help="The IP address to query", type=str)

ARGS = PARSER.parse_args()


def read_pcap(file):
    """
    Reads a pcap or pcapng file and gets all of the unique IP addresses

    :return: list of unique IP addresses
    """

    ips = {}  # a dictionary to hold all unique IP addresses and their count

    for p in PcapReader(file):
        if IP in p:  # check for an IP packet
            if not p[IP].src in ips:
                ips[p[IP].src] = 1
            else:
                ips[p[IP].src] += 1

    return [key for key in ips.keys()]


def get_ip_info(ip_address):
    """
    Gets info on the specified IP address such as geo-location

    :param ip_address: The IP address to query
    :return:
    """

    ip = ipaddress.ip_address(ip_address)
    ip_type = None

    if ip.is_loopback:
        ip_type = 'loopback'
    elif ip.is_multicast:
        ip_type = 'multicast'
    elif ip.is_reserved:
        ip_type = 'reserved'
    elif ip.is_private:
        ip_type = 'private'

    if not ip.is_global:
        raise ValueError('IP address is {}'.format(ip_type))

    response = requests.get('http://ip-api.com/json/' + ip_address, timeout=10)
    data = response.json()

    if data['status'] != 'success':  # something went wrong
        raise RuntimeError('Failed querying IP address ' + ip_address + ': ' + data['status'])

    return data


def main():
    if ARGS.file and ARGS.ipaddr:
        exit('Choose only one argument')
    elif not ARGS.file and not ARGS.ipaddr:
        PARSER.print_help()
        exit('Not enough arguments')

    if ARGS.file and not ARGS.ipaddr:
        limit = 150  # the API automatically bans your IP if you send over 150 requests per minute
        start = time.time()
        for counter, ip in enumerate(read_pcap(ARGS.file)):  # using enumerate for a counter
            if not counter + 1 % limit:
                sleep = start + 60 - time.time()
                if sleep > 0:
                    time.sleep(sleep)
                start = time.time()
            ip_info = get_ip_info(ip)
            print(json.dumps(ip_info, indent=4, sort_keys=True))
            print('\n')
    elif ARGS.ipaddr and not ARGS.file:
        ip_info = get_ip_info(ARGS.ipaddr)
        print(json.dumps(ip_info, indent=4, sort_keys=True))


if __name__ == '__main__':
    main()
